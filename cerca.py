# -*- coding: utf-8 -*-
"""
Created on Sat Dec 15 20:16:59 2018

@author: Pachacho
"""

import sys
import sqlite3
import argparse
import urllib.request as req
from ast import literal_eval
from datetime import datetime
from functions import parse_db, query_as_html, haversine as dist


def str_to_logical(key):
    """Reduces a key string expression to a logical AND/OR expression."""
    if isinstance(key, list):
        return "(" + " AND ".join(map(str_to_logical, key)) + ")"
    elif isinstance(key, tuple):
        return "(" + " OR ".join(map(str_to_logical, key)) + ")"
    else:
        return key


def prepare_input(args):
    """Checks if the key, date and distance values are properly defined."""

    try:

        key = str_to_logical(literal_eval(args.key))

        if args.date is None:
            date = datetime.now()
        else:
            date = datetime.strptime(args.date, "%d/%m/%Y")

        if args.distance is None:
            distance = 500
        else:
            distance = args.distance

    except SyntaxError as e:
        s = "\nSyntaxError: " \
            + "El valor de 'key' no es correcte. Comprova si esta ben escrit."

        sys.exit(s)

    return (key, date, distance)


def check_connectivity():
    try:
        req.urlopen('http://216.58.192.142', timeout=1)
    except req.URLError as err:
        sys.exit("\nURLError: La conexio a internet no funciona")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
            description='Cerca de esdeveniments i estacions de bicing.')

    # Add --key as mandatory argument
    parser.add_argument("--key", metavar="consulta", type=str,
                        help="llista de conjuncions i/o disjuncions",
                        required=True)

    # Add --date option
    parser.add_argument("--date", metavar="data", type=str,
                        help="format dd/mm/aaaa. Per defecte es avui.")

    # Add --distance option
    parser.add_argument(
        "--distance", metavar="distancia", type=float,
        help="Cerca estacions de bicing properas. Per defecte es 500 metres.")

    args = prepare_input(parser.parse_args())

    # Execute the query feed if it's first time, then the query
    get_all_tables = "SELECT * FROM sqlite_master WHERE type='table'"

    conn = sqlite3.connect("data.db")

    any_tables = conn.execute(get_all_tables).fetchall()

    if any_tables == []:
        check_connectivity()

        print("Creating database...")
        parse_db(conn)
        conn.commit()

    conn.create_function("DIST", 4, dist)
    conn.commit()

    print("Fetching all data asked...")
    print(args)
    query_as_html(conn, *args)

    conn.close()
