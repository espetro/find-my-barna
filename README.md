# Treball obligatory de Python per LP: Cercador de esdeveniments i stacions de bicing

*Scripts programats per Joaquin Terrasa Moya*

*cerca.py* es un script en Python que permetre la cerca mitjançant paraules
clau de esdeveniments a la ciutat de Barcelona, aixi com les parades de bicing
que hi ha a prop de cada esdeveniment.

La implementació s'ha fet utilitzant només la llibreria estandar de Python 3.6.
En línies generals, el programa recull informació web dels esdeveniments i les
estacions, i introdueix la informació a una base de dades. Després, qualsevol
petició es fa a la base de dades, que retorna la resposta en forma de taula HTML.

**Qué falta per fer:**
    + Normalitzar el llenguatge: Angles, castella i catala són utilizats.
    + La seleccio de les dates no esta be (hi ha que calcular la data de inici i de fi {FROM ___ TO ___})
    + Comprovar si la funcion de la distancia esta be
    + Repassar si el format compleix totes els requisits de PEP8
    + El script pot ser utilitzat sense conexio. Cal destacar les principals avantatges del meu programa.
    + Les distancias no estan gairebe calculats

Com ultim, estaria interesant comprovar quins resultats retorna depenent dels camps usats.

#### Bibliografia recurrente

    * https://www.w3schools.com
    * https://docs.python.org/3/library/sqlite3.html
    * https://www.python.org/dev/peps/pep-0008/
    * https://stackoverflow.com/questions/tagged/python
