#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 13 16:35:35 2018
@author: joaquin.terrasa
"""

import sqlite3
import tempfile as tmp
import webbrowser as web
import urllib.request as req
import xml.etree.ElementTree as ET
from math import sin, cos, sqrt, atan2, radians, asin
from datetime import datetime


def haversine(lat1, lon1, lat2, lon2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles
    return c * r * 1000



def parse_db(cursor):
    """Given both stations and events URLs, parse all XML data and
       stores it in a small SQLite Database"""

    bike_url = "https://wservice.viabicing.cat/v1/getstations.php?v=1"
    event_url = "http://w10.bcn.es/APPS/asiasiacache/peticioXmlAsia?id=199"

    # Creates BIKES table
    cursor.execute(
        """CREATE TABLE bikes
        (id real, lat real, long real, slots real, bikes real)""")
    cursor.commit()

    with req.urlopen(bike_url) as f:
        root = ET.parse(f).getroot()

    # Iterates over the XML to extract the required tags for each station
    tags = ["id", "lat", "long", "slots", "bikes"]

    stations_info = []
    for station in root.iter('station'):
        stations_info.append(
            tuple(map(lambda x: float(station.find(x).text), tags))
        )

    # Feeds the whole array into the DB
    cursor.executemany("INSERT INTO bikes VALUES (?,?,?,?,?)", stations_info)
    cursor.commit()

    # Creates EVENTS table
    cursor.execute(
        """CREATE TABLE events (id text, nom text, districte text,
                                data timestamp, carrer text,
                                lat real, long real)""")

    # Creates T_EVENTS virtual table for full-text search
    cursor.execute("""CREATE VIRTUAL TABLE t_events USING
                   FTS5(id, nom, barri, carrer)""")

    cursor.commit()

    with req.urlopen(event_url) as f:
        root = ET.parse(f).getroot().find("body/resultat/actes")

    # Iterates over the XML to get the needed tags for each event
    loc_tag = "lloc_simple/adreca_simple"

    tags = ["id", "nom", "{0}/districte".format(loc_tag),
            "data/data_proper_acte"]

    street_tag = [loc_tag+"/carrer", loc_tag+"/numero", loc_tag+"/codi_postal"]

    events_info = []

    # Adds every event info up
    for event in root:
        geo_pos = event.find("{}/coordenades/googleMaps".format(loc_tag)) \
            .attrib.values()

        # Some geolocation GMaps data may be corrupted (found '' at lat)
        if all(map(lambda v: bool(v.strip()), geo_pos)):
            info1 = list(map(lambda c: event.find(c).text, tags))
            # need to add "hora_fi"
            info2 = [' '.join(map(lambda c: str(event.find(c).text),
                                  street_tag))]

            info3 = list(map(lambda g: float(g), geo_pos))

            # Corrects badly formatted year data (adding current year)
            # SWITCH if it distortionates 2018-2019 data
            if int(info1[3][6:10]) > datetime.now().year:
                info1[3] = info1[3][:6] \
                           + str(datetime.now().year) + info1[3][10:]

            info1[3] = datetime.strptime(info1[3], "%d/%m/%Y %H.%M")

            events_info.append(tuple(info1 + info2 + info3))

            cursor.execute("INSERT INTO t_events VALUES (?,?,?,?)",
                           info1[0:3] + info2)

            cursor.commit()

    # Feeds the whole array into the DB
    cursor.executemany("INSERT INTO events VALUES (?,?,?,?,?,?,?)",
                       events_info)

    cursor.commit()


def query_as_html(conn, terms, date, distance):
    """Search the requested params into the DB and print it as an HTML table"""

    # HTML table template
    template_table = """
    <html>
        <head>
            <style>
                table {
                    border: 1px solid black;
                }
            </style>
        </head>
        <body>
            <h2>Eventos disponibles</h2>
            <table style="width:100%">
                <tr>
                    <th>Nom</th>
                    <th>Direccio</th>
                    <th>Data</th>
                    <th>Estacions amb llocs lliure</th>
                    <th>Estacions amb bicicletas</th>
                </tr>
      """

    # Retrieve all matching events
    main_query = """SELECT events.* FROM events
        INNER JOIN t_events ON events.id = t_events.id
        WHERE t_events MATCH ? AND events.data >= ?"""

    q1 = """SELECT id, lat, long, DIST(lat,long,?,?) as dist
            FROM bikes WHERE slots > 0 AND dist <= ?
            ORDER BY dist DESC LIMIT 5"""

    q2 = """SELECT id, lat, long, DIST(lat,long,?,?) as dist
            FROM bikes WHERE bikes > 0 AND dist <= ?
            ORDER BY dist DESC LIMIT 5"""

    template_station = "<tr><td>Estacio {} located at ({},{}) {}</tr></td>\n"

    # Retrieve all events related to the terms and date given
    events = conn.execute(main_query, (terms, date)).fetchall()

    for event in events[1:10]:
        # Obtener las estaciones mas cercanas con espacio
        print("-----------\nEvento: {}\n".format(event))
        print(event[-2:])
        params = (*event[-2:], distance)

        stations_empty = conn.execute(q1, params).fetchall()

        # Obtener las estaciones mas cercanas con bicis
        stations_bikes = conn.execute(q2, params).fetchall()
        print(stations_bikes)

        # Agrega cada estacion a la tabla HTML
        table_empty = ""
        for station in stations_empty:
            table_empty += template_station.format(*station)

        table_bikes = ""
        for station in stations_bikes:
            table_bikes += template_station.format(*station)

        # Agrega las dos tablas a la fila del evento
        template_table += """
        <tr>
            <td>{nom}</td>
            <td>{dir}</td>
            <td>{date}</td>
            <td>
                <table>
                    {slots}
                </table>
            </td>
            <td>
                <table>
                    {bikes}
                </table>
            </td>
        </tr>
        """.format(nom=event[1], dir=event[-3], date=event[3],
                   slots=table_empty, bikes=table_bikes)

    template_table += "</table></body></html>"

    # Opens the HTML table on a the default web browser
    print("All done! See the results in your web browser.")
    with tmp.NamedTemporaryFile(mode="w", encoding="UTF-8",
                                suffix=".html", delete=False) as f:
        f.write(template_table)

    web.open("file://{0}".format(f.name))
